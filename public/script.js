document.getElementById('testRunnerForm').addEventListener('submit', async function(event) {
    event.preventDefault();

    const state = document.getElementById('state').value;
    const environment = document.getElementById('environment').value;

    const response = await fetch('/run-test', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ state, environment})
    });

    const result = await response.text();
    document.getElementById('output').innerText = result;
});
